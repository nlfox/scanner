# -*- coding: utf-8 -*-
import subprocess
from gearman import GearmanWorker
import json
from scan import Scanner,NmapScanner
import zlib
import time
def crawl(job):
    #gearman工作函数
    print "received job:%s" % job.handle
    scan = NmapScanner(job.arg)
    try:
        scan.scan()
    except:
        raise Exception("err")
    report = scan.getReport()
    print report
    return zlib.compress(json.dumps(report))

if __name__=='__main__':
    worker = GearmanWorker(['127.0.0.1'])
    print "worker started."
    worker.register_function('crawl', crawl)
    worker.work()