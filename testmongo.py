import pymongo
from libnmap.parser import NmapParserException
from libnmap.parser import NmapParser
from libnmap.process import NmapProcess

# global_options = '--script=banner -sV -p T:21-25,80-89,110,143,443,513,873,1080,1433,1521,1158,3306-3308,3389,3690,5900,6379,7001,8000-8090,9000,9418,27017-27019,50060,111,11211,2049'
#
# def do_scan(targets, options):
#     parsed = None
#     nmproc = NmapProcess(targets, options)
#     rc = nmproc.run()
#     if rc != 0:
#         print("nmap scan failed: {0}".format(nmproc.stderr))
#     try:
#         parsed = NmapParser.parse(nmproc.stdout)
#     except NmapParserException as e:
#         print("Exception raised while parsing scan: {0}".format(e.msg))
#     return parsed
#
# report=do_scan('127.0.0.1',global_options)
# for host in report.hosts:
#     for service in host.services:
#         print service.service
