# coding=utf8
# client.py:启动client
import zlib
import sys
from gearman import GearmanClient
from gearman.task import Task, Taskset
from netaddr import IPNetwork
import pymongo
import json

tasks = Taskset()

conn = pymongo.MongoClient('localhost', 27017)
col = conn.scanner.report

# register callbacks to response
class MyTask(Task):
    def __init__(self, func, arg, uniq=None,timeout=None, retry_count=0):
        Task.__init__(self, func, arg, uniq,timeout,retry_count)

    def complete(self, result):
        result = zlib.decompress(result)
        print "task handle:%s ,result:%s" % (self.handle, result[0:50])
        result = json.loads(result)
        #去重。
        t = col.find_one({"target": result['target']})
        if t:
            col.remove({"_id":t['_id']})
        col.insert(result)
        self._finished()

    def status(self, numerator, denominator):
        # todo:增加分段显示，所有都是fail
        print "task uniq:%s ,status:%d/%d" % (self.uniq,numerator,denominator)
        self._finished()
#       去掉失败的回调
#     def fail(self):
#         if not self._finished():
#             print 'failed %s' % (self.arg)
# #            self._finished()

def getIP(ip):
    ipList = []
    for i in IPNetwork(ip):
        ipList.append(i)
    return ipList

if __name__ == '__main__':
    try:
        ip = sys.argv[1]
    except:
        ip = '202.118.66.59/30'
    print ip
    ipList = getIP(ip)
    client = GearmanClient(['127.0.0.1'])
    # JUST FOR TEST
    for i in ipList:
        t = MyTask('crawl', i, str(i))
        tasks.add(t)
        print "add task,unique:%s  handle:%s" % (t.uniq, t.handle)
        i += 1
    # run the tasks in parallel
    print "do tasks:"
    if client.do_taskset(tasks):
        print 'over'
