# -*- coding: utf-8 -*-
import json
import logging

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] %(name)s:%(levelname)s: %(message)s"
)
__author__ = 'nlfox'
from libnmap.parser import NmapParser, NmapParserException
from libnmap.process import NmapProcess
import os
import urllib2
import sys


class Scanner:
    def __init__(self, target):
        self.target = target
        self.report = {}
        self.plugins = []

    def scan(self):
        pass

    def loadPlugins(self):
        pass

    def scanService(self):
        pass

    def getReport(self):
        return self.report

    def pluginScan(self, service):
        pass


class NmapScanner(Scanner):
    def __init__(self, target):
        Scanner.__init__(self, target)
        self.global_options = '--script=banner -sV -p T:21-25,80-89,110,143,443,513,873,1080,1433,1521,1158,3306-3308,3389,3690,5900,6379,7001,8000-8090,9000,9418,27017-27019,50060,111,11211,2049'
        self.plugins = []
        #暂时不启用
        self.domains = []

    def loadPlugins(self):
        for filename in os.listdir("plugins"):
            if not filename.endswith(".py") or filename.startswith("_"):
                continue
            pluginName = os.path.splitext(filename)[0]
            p = __import__("plugins." + pluginName, fromlist=["MyPlugin"])
            self.plugins.append(p)

    def getReverseIP(self):
        try:
            logging.debug('IPget' + self.target)
            ip = self.target
            url = 'http://dns.aizhan.com/index.php?r=index/domains&ip=%s' % ip
            data = urllib2.urlopen(url,timeout=2).readline()
            result = []
            if not json.loads(data).has_key('domains'):
                return result
            list = json.loads(data)
            count = list['count']
            page = 1
            maxpage = list['maxpage']
            if maxpage > page:
                for p in range(page, maxpage + 1):
                    purl = 'http://dns.aizhan.com/index.php?r=index/domains&ip=%s&page=%s' % (str(ip), p)
                    data2 = urllib2.urlopen(purl,timeout=2).readline()
                    list2 = json.loads(data2)
                    for d in list2['domains']:
                        result.append(d)
            else:
                for d in list['domains']:
                    result.append(d)
        except:
            result = []
        self.domains = result


    def getServiceList(self):
        # 扫描服务列表
        def do_scan(targets, options):
            parsed = None
            nmproc = NmapProcess(targets, options)
            rc = nmproc.run()
            if rc != 0:
                print("nmap scan failed: {0}".format(nmproc.stderr))
            try:
                parsed = NmapParser.parse(nmproc.stdout)
            except NmapParserException as e:
                print("Exception raised while parsing scan: {0}".format(e.msg))
            return parsed

        nmap_report = do_scan(self.target, self.global_options)
        report = {}
        report['target'] = self.target
        report['service'] = []
        for host in nmap_report.hosts:
            logging.debug('host get')
            report['target'] = self.target
            servArr = []
            for serv in host.services:
                servArr.append({'port': serv.port, 'service': serv.service,'banner':serv.banner, 'pluginReport': []})
            report['service'] = servArr
        if report['service'] == []:
            raise Exception("failed")
        self.report = report


    def getWebServList(self):
        #if domains != []""
        for domain in self.domains:
            logging.debug('service')
            self.report['service'].append({'service': 'www', 'domain': 'http://' + domain + '/', 'pluginReport': []})
        for service in self.report['service']:
            if service['service'] == 'http' or service['service'] == 'https':
                self.report['service'].append(
                    {'service': 'www', 'domain': service['service'] + "://" + self.target + "/", 'pluginReport': []})

                # self.report['webService'] = domains

    def scan(self):
        # 扫描主过程抽象
        self.getServiceList()
        logging.debug('1')
        #self.getReverseIP()
        self.getWebServList()
        #self.loadPlugins()
        #self.scanService()

    def scanService(self):
        # 扫描服务
        services = self.report['service']
        for service in services:
            pluginReport = self.pluginScan(service)
            service['pluginReport'] = pluginReport

    def pluginScan(self, service):
        # 通过插件扫描
        pluginReport = []
        for p in self.plugins:
            # todo:强行兼容bugscan 然而并不靠谱
            pResult = []
            if service.has_key('domain') and p.assign('www', service['domain']):
                p.audit(p.assign('www', service['domain']))
                pluginReport.append(pResult)
                continue
            if service.has_key('domain'):
                continue
            if p.assign(service["service"], (self.target, int(service["port"]))):
                p.audit(p.assign(service["service"], (self.target, int(service["port"])))[1])
                pluginReport.append(pResult)
        return pluginReport
        #print service
