# -*- coding: utf-8 -*-
# __author__ = 'nlfox'
# from plugins import Plugin
# class MyPlugin(Plugin):
#     name = "firstPlugin"
#     def __init__(self, target, port, service):
#         Plugin.__init__(self, target, port, service)
#
#     def run(self):
#         self.check()
#         self.result.append('result from my first plugin')
#
#     def check(self):
#         if 0:
#             return True
#         else:
#             return False
#!/usr/bin/env python

"""
POC Name  : redis未授权访问
Author    : a
mail      :a@lcx.cc
"""
import socket

from plugins.dummy import *


def assign(service, arg):
    if service == 'redis':
        return True, arg

def audit(arg):
    ip,port = arg
    payload = '\x2a\x31\x0d\x0a\x24\x34\x0d\x0a\x69\x6e\x66\x6f\x0d\x0a'
    try:
        s = socket.socket()
        s.connect((ip,port))
        s.send(payload)
        data = s.recv(1024)
        if 'redis_version' in data:
            security_hole( ip + ':' + str(port) + 'redis未授权访问')
        s.close()
    except:
        pass
