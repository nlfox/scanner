#!/usr/bin/env python
# -*- coding: utf-8 -*-
#author:range
#refer:http://linux.im/2015/05/07/jQuery-1113-DomXSS-Vulnerability.html
#refer:http://www.beebeeto.com/pdb/poc-2015-0097/
import chardet
import urllib2
import zlib
from plugins.dummy import *

def assign(service, arg):
    if service == "www":
        return True, arg

def audit(arg):
    verify_url = arg
    code, head, res, _, _ = curl.curl(verify_url)
    try:
        res=urllib2.urlopen(verify_url[1],timeout=2)
        resContent = res.read()
        charset = chardet.detect(resContent)['encoding']
        resContent = resContent.decode(charset)
        resHeaders = res.info()
        res = str(resHeaders) + resContent
    except :
        res = 'error'
    security_hole([verify_url[1], res])
